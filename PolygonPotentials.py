# coding=utf-8

"""
PolygonPotentials.py
Used to return the potential caused by a finite polygon defined by the points on the edges for a given depth and
voltage.
"""

from __future__ import division

from numpy import arctan2, sqrt, pi, cos, sin, arccos

from Triangulate import decompose_polygon

__author__ = "Tobias Bautze and Ryan Moodie"
__date__ = "10 Aug 2015"


class InfiniteTriangle(object):
    """
    Calling this function returns the potential caused by an infinite triangle at the origin with given angle and given
    applied voltage at a certain depth depth. This is found using a solution of Poisson's equation.
    """

    def __init__(self, voltage, depth, angle):
        self.voltage = voltage
        self.depth = depth
        self.angle = angle

    def __call__(self, position):
        x, y = position
        voltage, depth, angle = (self.voltage, self.depth, self.angle / 2)
        r = sqrt(depth ** 2 + x ** 2 + y ** 2)
        return voltage / pi * arctan2(depth * sin(angle), (r - x) * cos(angle) - y * sin(angle))


class ArbInfiniteTriangle(object):
    """
    Calling this function returns the potential caused by an infinite triangle defined by 3 points with the enclosing
    angle given by the middle point and given applied voltage at a certain depth depth.
    Unlike the Polygon this function only accepts clockwise ordered Points.
    """

    def __init__(self, voltage, depth, points_x, points_y):
        self.voltage = voltage
        self.depth = depth
        self.points_x = points_x
        self.points_y = points_y
        self.angle = self.vertex_angle(self.points_x, self.points_y)
        self.base_angle = self.vertex_base_angle(self.points_x, self.points_y)
        self.cosine_of_base = cos(self.base_angle)
        if (points_y[0] - points_y[1]) <= 0:
            self.sine_of_base = sin(self.base_angle)
        else:
            self.sine_of_base = -sin(self.base_angle)

    @staticmethod
    def vertex_angle(vertex_x, vertex_y):
        scalar = ((vertex_x[0] - vertex_x[1]) * (vertex_x[2] - vertex_x[1])) + (
            (vertex_y[0] - vertex_y[1]) * (vertex_y[2] - vertex_y[1]))
        l1 = sqrt((vertex_x[0] - vertex_x[1]) ** 2 + (vertex_y[0] - vertex_y[1]) ** 2)
        l2 = sqrt((vertex_x[2] - vertex_x[1]) ** 2 + (vertex_y[2] - vertex_y[1]) ** 2)
        angle = arccos(scalar / (l1 * l2))
        return angle

    @staticmethod
    def vertex_base_angle(vertex_x, vertex_y):
        scalar = (vertex_x[0] - vertex_x[1]) * (1) + (vertex_y[0] - vertex_y[1]) * (0)
        length = (sqrt((vertex_x[0] - vertex_x[1]) ** 2 + (vertex_y[0] - vertex_y[1]) ** 2))
        angle = arccos(scalar / length)
        return angle

    def move_coordinates(self, x, y):
        """
        transforms the investigated point such that the calculation works with InfiniteTriangle
        """
        x1 = x - self.points_x[1]
        y1 = y - self.points_y[1]
        new_x = x1 * self.cosine_of_base - y1 * self.sine_of_base
        new_y = x1 * self.sine_of_base + y1 * self.cosine_of_base
        return new_x, new_y

    def __call__(self, pos):
        x, y = pos
        triangle1 = InfiniteTriangle(self.voltage, self.depth, self.angle)
        return triangle1(self.move_coordinates(x, y))


class ArbTriangle(object):
    """
    Calling this function returns the potential caused by a finite triangle defined by the points on the corners and
    applied voltage at a certain depth depth. The finite triangle is created by 3 infinite triangles. The starting point is
    the corner at the right angle. The position of the point is given by 2 Lists, the first for the X coordinates, the
    second for Y coordinates. Unlike the polygon this function only accepts clockwise ordered points.
    """

    def __init__(self, voltage, depth, points_x, points_y):
        self.voltage = voltage
        self.depth = depth
        self.points_x = points_x
        self.points_y = points_y

    def __call__(self, position):
        # Points for infinite Triangle between vector P0-P1 and P2-P1
        triangle2x = [2 * self.points_x[0] - self.points_x[1], self.points_x[0], self.points_x[2]]
        triangle2y = [2 * self.points_y[0] - self.points_y[1], self.points_y[0], self.points_y[2]]
        # Points for infinite Triangle between vector P2-P1 and P2-P0
        triangle3x = [2 * self.points_x[2] - self.points_x[1], self.points_x[2],
                      2 * self.points_x[2] - self.points_x[0]]
        triangle3y = [2 * self.points_y[2] - self.points_y[1], self.points_y[2],
                      2 * self.points_y[2] - self.points_y[0]]
        # Create infinite triangles
        triangle1 = ArbInfiniteTriangle(self.voltage, self.depth, self.points_x, self.points_y)
        triangle2 = ArbInfiniteTriangle(self.voltage, self.depth, triangle2x, triangle2y)
        triangle3 = ArbInfiniteTriangle(self.voltage, self.depth, triangle3x, triangle3y)
        return triangle1(position) - triangle2(position) + triangle3(position)


class Polygon(object):
    """
    Calling this function returns the potential caused by a finite polygon defined by the points on the edges for a
    given depth and voltage. The points are given by 2 Lists of X and Y coordinates respectively. The ordering of the
    points can be clockwise or counterclockwise.
    """

    @staticmethod
    def cut_ears(points_x, points_y):
        """
        Cuts apart the Polygon into a list of triangles. The returned triangles are ordered clockwise.
        """
        v = zip(points_x, points_y)
        triangles1 = decompose_polygon(v)
        triangles = zip(triangles1[0], triangles1[1])
        return triangles

    def __init__(self, voltage, depth, points):
        self.voltage = voltage
        self.depth = depth
        self.points_x = points[:, 0]
        self.points_y = points[:, 1]
        self.triangles = self.cut_ears(self.points_x, self.points_y)

    def __call__(self, position):
        potential = 0
        for triangle in self.triangles:
            tri = ArbTriangle(self.voltage, self.depth, triangle[0], triangle[1])
            potential = potential + tri(position)
        return potential
