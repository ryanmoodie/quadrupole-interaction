# coding=utf-8

"""
DistancesArrays.py
Calculates distances between all points in the two solving regions and stores them in two arrays, one for each solving
region.
"""

from numpy import sqrt, empty

__author__ = "Ryan Moodie"
__credits__ = "Dr Brendon Lovett"
__date__ = "14 Aug 2015"


def create_distances_arrays(settings):
    distances_array_0_nm = empty(
        [settings.column_size_grid_0, settings.row_size_grid_0, settings.column_size_grid_1, settings.row_size_grid_1],
        dtype=float)

    distances_array_1_nm = empty(
        [settings.column_size_grid_1, settings.row_size_grid_1, settings.column_size_grid_0, settings.row_size_grid_0],
        dtype=float)

    for b in xrange(settings.column_size_grid_0):
        for a in xrange(settings.row_size_grid_0):
            for y in xrange(settings.column_size_grid_1):
                for x in xrange(settings.row_size_grid_1):
                    distances_array_0_nm[b, a, y, x] = sqrt(
                        (settings.start_x_nm_1 + settings.lattice_constant_nm * x - settings.start_x_nm_0
                         - settings.lattice_constant_nm * a) ** 2 + (
                            settings.start_y_nm_1 + settings.lattice_constant_nm * y
                            - settings.start_y_nm_0 - settings.lattice_constant_nm * b) ** 2)

                    distances_array_1_nm[y, x, b, a] = distances_array_0_nm[b, a, y, x]

    return distances_array_0_nm, distances_array_1_nm
