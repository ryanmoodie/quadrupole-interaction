# coding=utf-8

"""
PotentialPlots.py
Plots potential with the two solving regions highlighted.
"""

from numpy import meshgrid, min, max
from matplotlib.pyplot import pcolormesh, colorbar, axis, title, xlabel, ylabel, gca, figure, xlim, ylim, text
from matplotlib.patches import Rectangle

__author__ = "Ryan Moodie and Hanno Bautze"
__date__ = "13 Aug 2015"


def plot_2d_map(simulation, settings):
    figure(settings.n)

    x, y = meshgrid(settings.x_axis_nm, settings.y_axis_nm)
    mat = simulation.potential

    mat_min = min(mat)
    mat_max = max(mat)

    pcolormesh(x, y, mat, vmin=mat_min, vmax=mat_max)

    colorbar().set_label('Potential [V]')
    axis('scaled')
    title('Potential Landscape')
    xlabel('x [nm]', fontsize=10)
    ylabel('y [nm]', fontsize=10)
    xlim(settings.left_border_nm, settings.right_border_nm - settings.lattice_constant_nm)
    ylim(settings.lower_border_nm, settings.upper_border_nm - settings.lattice_constant_nm)

    settings.end_figure()


def highlight_regions(settings):
    ax = gca()

    ax.add_patch(
        Rectangle((settings.start_x_nm_0, settings.start_y_nm_0), settings.row_size_nm_0, settings.column_size_nm_0,
                  fill=False, edgecolor='white'))
    ax.add_patch(
        Rectangle((settings.start_x_nm_1, settings.start_y_nm_1), settings.row_size_nm_1, settings.column_size_nm_1,
                  fill=False, edgecolor='white'))

    text(settings.start_x_nm_0 + 1, settings.start_y_nm_0 + 1, str(0), color='white')
    text(settings.start_x_nm_1 + 1, settings.start_y_nm_1 + 1, str(1), color='white')
