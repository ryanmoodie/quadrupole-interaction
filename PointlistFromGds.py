# coding=utf-8

"""
PointlistFromGds.py
Reads GDS file and outputs pointlist.
"""

from __future__ import division

from re import split
from numpy import array, reshape
from gdsii import types
from gdsii.record import Record

__author__ = "Tobias Bautze, Hanno Flentje and Ryan Moodie"
__date__ = "10 Aug 2015"


class PointlistFromGds(object):
    """
    Interface to convert the polygons from GDS files into point list that can be used to calculate the potential
    landscapes. Reads gds file. Outputs pointlist when called.
    """

    def __init__(self, file_name):
        self.file_name = file_name
        self.units = []
        self.point_list = []

    @staticmethod
    def show_data(rec):
        if rec.tag_type == types.BITARRAY:
            return str(rec.data)
        return ', '.join('{0}'.format(i) for i in rec.data)

    def run(self):
        """
        # Open filename (if exists). Read units. Get list of polygons.
        """
        with open(self.file_name, 'rb') as a_file:
            for rec in Record.iterate(a_file):
                if rec.tag_type == types.NODATA:
                    pass
                else:
                    if rec.tag_name == 'UNITS':
                        unit_string = self.show_data(rec)
                        self.units = array(split(',', unit_string)).astype(float)
                    elif rec.tag_name == 'XY':
                        # get data
                        data_string = self.show_data(rec)
                        # split string at , and convert to float
                        data = array(split(',', data_string)).astype(float)
                        # reshape into [[x1,y1],[x2,y2],...]
                        data = reshape(data, (len(data) / 2, 2))[:-1]
                        self.point_list.append(data)
        return array(self.point_list) * self.units[1]
