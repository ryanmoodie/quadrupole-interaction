# coding=utf-8

"""
SchrodingerSolver.py
Creates a tight binding square lattice system with fed potential. Solves system for eigenvalues and wavefunctions.
"""

from __future__ import division

from numpy import abs, reshape, zeros
from matplotlib.pyplot import xlabel, ylabel, title, gca, figure, savefig
from scipy.sparse.linalg import eigsh
from kwant import plotter, lattice, Builder

__author__ = "Ryan Moodie"
__credits__ = ["Dr Brendon Lovett", "Giuseppe Pica", "Pierre-Andre Mortemousque"]
__date__ = "13 Aug 2015"


def cutout_potential(simulation, settings, number):
    start_x = settings.start_x_potential_grid_1 if number else settings.start_x_potential_grid_0
    end_x = settings.end_x_potential_grid_1 if number else settings.end_x_potential_grid_0
    start_y = settings.start_y_potential_grid_1 if number else settings.start_y_potential_grid_0
    end_y = settings.end_y_potential_grid_1 if number else settings.end_y_potential_grid_0

    return simulation.potential[start_y:end_y + 2, start_x:end_x + 2]


def make_lattice(settings, number):
    start_x = settings.start_x_nm_1 if number else settings.start_x_nm_0
    start_y = settings.start_y_nm_1 if number else settings.start_y_nm_0

    square_lattice = lattice.square(settings.lattice_constant_nm)
    square_lattice.offset = [start_x, start_y]

    return square_lattice


def make_system(potential, square_lattice, settings, number):
    t = settings.hopping_constant_meV

    row_size = settings.row_size_grid_1 if number else settings.row_size_grid_0
    column_size = settings.column_size_grid_1 if number else settings.column_size_grid_0

    system = Builder()

    for y in xrange(column_size):
        for x in xrange(row_size):
            system[square_lattice(x, y)] = 4 * t + potential[y, x]

    system[square_lattice.neighbors()] = -t

    return system


def make_empty_system(square_lattice, settings, number):
    t = settings.hopping_constant_meV

    row_size = settings.row_size_grid_1 if number else settings.row_size_grid_0
    column_size = settings.column_size_grid_1 if number else settings.column_size_grid_0

    system = Builder()

    for y in xrange(column_size):
        for x in xrange(row_size):
            system[square_lattice(x, y)] = 4 * t

    system[square_lattice.neighbors()] = -t

    return system


def solve_hamiltonian(system, settings, number):
    state = settings.state_1 if number else settings.state_0

    hamiltonian_matrix = system.hamiltonian_submatrix(sparse=True)

    eigenvalues, eigenvectors = eigsh(hamiltonian_matrix, k=state + 1, which='SA')

    eigenvector = eigenvectors[:, state]
    eigenvalue = eigenvalues[state]

    return eigenvalue, eigenvector


def calculate_probability_density(wavefunction, settings, number):
    row_size = settings.row_size_grid_1 if number else settings.row_size_grid_0
    column_size = settings.column_size_grid_1 if number else settings.column_size_grid_0

    probability_density = abs(wavefunction) ** 2

    probability_density_matrix = reshape(probability_density, (column_size, row_size))

    return probability_density, probability_density_matrix


def solve_initial_region(system, settings, number):
    system = system.finalized()
    new_eigenvalue, wavefunction = solve_hamiltonian(system, settings, number)
    probability_density, probability_density_matrix = calculate_probability_density(wavefunction, settings, number)

    return new_eigenvalue, wavefunction, probability_density, probability_density_matrix, system


def solve_region(other_probability_density_matrix, original_potential, square_lattice, distances_array, settings,
                 number):
    row_size = settings.row_size_grid_1 if number else settings.row_size_grid_0
    column_size = settings.column_size_grid_1 if number else settings.column_size_grid_0
    row_size_other = settings.row_size_grid_0 if number else settings.row_size_grid_1
    column_size_other = settings.column_size_grid_0 if number else settings.column_size_grid_1

    additional_potential = zeros([column_size, row_size], dtype=float)
    new_potential = zeros([column_size, row_size], dtype=float)

    for b in xrange(column_size):
        for a in xrange(row_size):
            for y in xrange(column_size_other):
                for x in xrange(row_size_other):
                    additional_potential[b, a] += settings.new_potential_factor_meV * other_probability_density_matrix[
                        y, x] / distances_array[b, a, y, x]

    for y in xrange(column_size):
        for x in xrange(row_size):
            new_potential[y, x] = original_potential[y, x] + additional_potential[y, x]

    system = make_system(new_potential, square_lattice, settings, number).finalized()
    new_eigenvalue, wavefunction = solve_hamiltonian(system, settings, number)
    probability_density, probability_density_matrix = calculate_probability_density(wavefunction, settings, number)

    return new_eigenvalue, wavefunction, probability_density, probability_density_matrix, system


def plot_function(system, plot_title, function, file_name, settings):
    figure(settings.n)
    ax = gca()
    title(plot_title)
    xlabel('x [nm]', fontsize=10)
    ylabel('y [nm]', fontsize=10)
    plotter.map(system, function, oversampling=1, ax=ax)

    if settings.save_plots:
        savefig(file_name, bbox_inches='tight')

    settings.end_figure()
