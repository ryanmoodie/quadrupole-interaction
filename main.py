#!/usr/bin/env python
# coding=utf-8

"""
main.py
File to be executed to run simulation.
"""

from __future__ import print_function

from sys import exit
from time import clock
from platform import system
from numpy import real, imag, round
from matplotlib.pyplot import subplot, show, savefig
from kwant import plot as kwant_plot

from Settings import Settings
from Simulation import Simulation
from ReadWriteToDisk import write_out, read_file, append_to_file
from PotentialPlots import plot_2d_map, highlight_regions
from SchrodingerSolver import cutout_potential, solve_initial_region, solve_region, plot_function, make_lattice, \
    make_system, make_empty_system
from DistancesArrays import create_distances_arrays
from QuadrupoleInteraction import find_quadrupole_moment_component, find_quadrupole_interaction_energy_automatic, \
    find_quadrupole_interaction_energy_manual

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__credits__ = ["Dr Brendon Lovett", "Giuseppe Pica", "Tobias Bautze", "Hanno Flentje", "Pierre-Andre Mortemousque",
               "Sebastian Santisi", "Michael Lawson"]
__date__ = "14 Aug 2015"

if __name__ == '__main__':

    settings = Settings()

    if settings.verbose:
        TIME_0 = clock()


        def stopwatch(initial_time):
            return round(clock() - initial_time, 3)

    simulation = Simulation()

    if settings.load_potential:

        if settings.verbose:
            TIME_1 = clock()
            print('Loading initial potential from disk...')

        simulation.read_potential(read_file(settings.potential_name))

        if settings.verbose:
            print('    Complete. (', stopwatch(TIME_1), 's )')

    else:

        if settings.verbose:
            TIME_2 = clock()
            print('Calculating initial potential...')

        simulation.evaluate_potential(settings)

        if settings.verbose:
            print('    Complete. (', stopwatch(TIME_2), 's )')

        if settings.save_potential:
            if settings.verbose:
                TIME_3 = clock()
                print('    Writing initial potential to disk...')

            write_out(simulation.potential, settings.potential_name)

            if settings.verbose:
                print('        Complete. (', stopwatch(TIME_3), 's )')

        if settings.show_gates:
            simulation.plot_gates(settings)

            if settings.verbose:
                print('    Gates plot prepared.')

                if settings.save_plots:
                    print('        Gates plot saved.')

    if settings.show_potential_only:

        plot_2d_map(simulation, settings)
        highlight_regions(settings)

        if settings.verbose:
            print('Showing potential plot with solving regions.\nTight binding lattice sizes are:\nRegion 0:',
                  settings.row_size_grid_0, 'x', settings.column_size_grid_0, '(', settings.number_of_sites_0,
                  'sites )\nRegion 1:', settings.row_size_grid_1, 'x', settings.column_size_grid_1, '(',
                  settings.number_of_sites_1, 'sites )\nWith physical dimensions of:\nRegion 0:',
                  "%.3g" % settings.row_size_nm_0, 'x', "%.3g" % settings.column_size_nm_0, 'nm\nRegion 1:',
                  "%.3g" % settings.row_size_nm_1, 'x', "%.3g" % settings.column_size_nm_1, 'nm\nSimulation complete.')

        show()
        exit()

    lattice_0 = make_lattice(settings, 0)
    lattice_1 = make_lattice(settings, 1)

    if settings.load_self_consistent_results:

        if settings.verbose:
            TIME_4 = clock()
            print('Loading self-consistent solver solutions...')

        system_0 = make_empty_system(lattice_0, settings, 0).finalized()
        system_1 = make_empty_system(lattice_1, settings, 1).finalized()

        wavefunction_0, wavefunction_1 = read_file(settings.wavefunctions_name)
        new_eigenvalue_0, new_eigenvalue_1 = read_file(settings.eigenvalues_name)
        probability_density_0, probability_density_1 = read_file(settings.probability_densities_name)
        probability_density_matrix_0, probability_density_matrix_1 = read_file(
            settings.probability_density_matrices_name)

        i = str('n/a')

        if settings.verbose:
            print('    Complete. (', stopwatch(TIME_4), 's )')

    else:

        original_potential_0 = cutout_potential(simulation, settings, 0)
        original_potential_1 = cutout_potential(simulation, settings, 1)

        system_0 = make_system(original_potential_0, lattice_0, settings, 0)

        if settings.load_distances_array:

            if settings.verbose:
                TIME_5 = clock()
                print('Loading distances arrays from disk...')

            distances_array_0, distances_array_1 = read_file(settings.distances_array_name)

            if settings.verbose:
                print('    Complete. (', stopwatch(TIME_5), 's )')

        else:

            if settings.verbose:
                TIME_6 = clock()
                print('Calculating distances arrays...')

            distances_array_0, distances_array_1 = create_distances_arrays(settings)

            if settings.verbose:
                print('    Complete. (', stopwatch(TIME_6), 's )')

            if settings.save_distances_array:

                if settings.verbose:
                    TIME_7 = clock()
                    print('    Writing distances arrays to disk...')

                write_out([distances_array_0, distances_array_1], settings.distances_array_name)

                if settings.verbose:
                    print('        Complete. (', stopwatch(TIME_7), 's )')

        if settings.verbose:
            TIME_8 = clock()
            print('Solving for initial wavefunction in region 0...')

        new_eigenvalue_0, wavefunction_0, probability_density_0, probability_density_matrix_0, system_0 = \
            solve_initial_region(system_0, settings, 0)

        if settings.verbose:
            print('    Complete. (', stopwatch(TIME_8),
                  's )\nSolving for wavefunction in region 1 with new potential due to region 0 wavefunction...')
            TIME_9 = clock()

        new_eigenvalue_1, wavefunction_1, probability_density_1, probability_density_matrix_1, system_1 = solve_region(
            probability_density_matrix_0, original_potential_1, lattice_1, distances_array_1, settings, 1)

        if settings.show_probability_densities_for_iterations:
            plot_function(system_0, new_eigenvalue_0, probability_density_0,
                          str('png/probability_density_0_iteration_0.png'), settings)
            plot_function(system_1, new_eigenvalue_1, probability_density_1,
                          'png/probability_density_1_iteration_0.png', settings)

        if settings.verbose:
            print('    Complete. (', stopwatch(TIME_9),
                  's )\nRepeating until self-consistent...\n    Values shown are:\n    Iteration number, percentage '
                  'differences in eigenvalues.')
            TIME_10 = clock()

        delta_0 = float(1000)
        delta_1 = float(1000)

        i = 1

        while delta_0 > settings.convergence_factor or delta_1 > settings.convergence_factor:
            if settings.verbose:
                TIME_11 = clock()

            old_eigenvalue_0 = new_eigenvalue_0
            old_eigenvalue_1 = new_eigenvalue_1

            new_eigenvalue_0, wavefunction_0, probability_density_0, probability_density_matrix_0, system_0 = \
                solve_region(
                    probability_density_matrix_1, original_potential_0, lattice_0, distances_array_0, settings, 0)

            new_eigenvalue_1, wavefunction_1, probability_density_1, probability_density_matrix_1, system_1 = \
                solve_region(
                    probability_density_matrix_0, original_potential_1, lattice_1, distances_array_1, settings, 1)

            delta_0 = 100 * abs(new_eigenvalue_0 - old_eigenvalue_0) / new_eigenvalue_0
            delta_1 = 100 * abs(new_eigenvalue_1 - old_eigenvalue_1) / new_eigenvalue_1

            if settings.show_probability_densities_for_iterations:
                plot_function(system_0, new_eigenvalue_0, probability_density_0,
                              settings.iteration_probability_densities_names(i, 0), settings)
                plot_function(system_1, new_eigenvalue_1, probability_density_1,
                              settings.iteration_probability_densities_names(i, 1), settings)

            if settings.verbose:
                print('    ', i, ':', "%.3g" % delta_0, '%,', "%.3g" % delta_1, '% (', stopwatch(TIME_11), 's )')

            i += 1

        if settings.verbose:

            print('    Converged after ', i - 1, 'repetitions. (', stopwatch(TIME_10), 's )')

            if settings.show_probability_densities_for_iterations:
                print('    Probability density plots for each iteration prepared.')

                if settings.save_plots:
                    print('        Probability density plots for each iteration saved.')

        if settings.save_wavefunctions:

            if settings.verbose:
                TIME_12 = clock()
                print('    Writing wavefunction solutions to disk...')

            write_out([wavefunction_0, wavefunction_1], settings.wavefunctions_name)
            write_out([new_eigenvalue_0, new_eigenvalue_1], settings.eigenvalues_name)
            write_out([probability_density_0, probability_density_1], settings.probability_densities_name)
            write_out([probability_density_matrix_0, probability_density_matrix_1],
                      settings.probability_density_matrices_name)

            if settings.verbose:
                print('        Complete. (', stopwatch(TIME_12), 's )')

    if settings.show_eigenvalues:
        print('    Eigenvalues:\n        Region 0:', "%.9g" % new_eigenvalue_0, 'meV.\n        Region 1:',
              "%.9g" % new_eigenvalue_1, 'meV.')

    if settings.calculate_quadrupole_interaction:

        if settings.verbose:
            TIME_13 = clock()
            print('Calculating quadrupole interactions...')

        quadrupole_moment_xx = find_quadrupole_moment_component('xx', probability_density_matrix_0,
                                                                probability_density_matrix_1, settings)
        quadrupole_moment_yy = find_quadrupole_moment_component('yy', probability_density_matrix_0,
                                                                probability_density_matrix_1, settings)

        if settings.show_quadrupole_interaction_results:
            print('    Quadrupole moment components:\n        Qxx =', "%.9g" % quadrupole_moment_xx,
                  'm^2.\n        Qyy =', "%.9g" % quadrupole_moment_yy, 'm^2.')

        if settings.align_external_potential_with_strongest_moment:
            direction, quadrupole_interaction_energy = find_quadrupole_interaction_energy_automatic(
                quadrupole_moment_xx, quadrupole_moment_yy, settings)

            if settings.show_quadrupole_interaction_results:
                print('    Quadrupole interaction energy:\n        ', "%.9g" % quadrupole_interaction_energy,
                      'eV,\n    with applied potential:\n        ', direction, '=',
                      "%.3g" % settings.external_potential, 'Vm^-2.')

        else:
            quadrupole_interaction_energy = find_quadrupole_interaction_energy_manual(quadrupole_moment_xx,
                                                                                      quadrupole_moment_yy, settings)
            if settings.show_quadrupole_interaction_results:
                print('    Quadrupole interaction energy:\n        ', "%.9g" % quadrupole_interaction_energy, 'eV.')

            direction = str('n/a')

        if settings.verbose:
            print('    Complete. (', stopwatch(TIME_13), 's )')

    else:
        quadrupole_moment_xx = str('n/a')
        quadrupole_moment_yy = str('n/a')
        quadrupole_interaction_energy = str('n/a')
        direction = str('n/a')

    if settings.show_probability_density_matrix:

        if settings.verbose:
            print('Printing probability densities for regions 0 and 1 respectively:')

        print(probability_density_matrix_0)
        print(probability_density_matrix_1)

    if settings.show_solving_region_plot:

        plot_2d_map(simulation, settings)
        ax = subplot(111)
        kwant_plot(system_0, ax=ax)
        kwant_plot(system_1, ax=ax)
        highlight_regions(settings)

        if settings.verbose:
            print('Potential plot with tight-binding systems prepared.')

        if settings.save_plots:

            savefig(settings.potential_plot_name, bbox_inches='tight')

            if settings.verbose:
                print('    Potential plot saved.')

    if settings.show_wavefunction_real:

        plot_function(system_0, 'Real part of wavefunction, region 0', real(wavefunction_0),
                      settings.wavefunction_real_plot_0_name, settings)
        plot_function(system_1, 'Real part of wavefunction, region 1', real(wavefunction_1),
                      settings.wavefunction_real_plot_1_name, settings)

        if settings.verbose:
            print('Wavefunction (real part) plots prepared.')

            if settings.save_plots:
                print('    Wavefunction (real part) plots saved.')

    if settings.show_wavefunction_imaginary:

        plot_function(system_0, 'Imaginary part of wavefunction, region 0', imag(wavefunction_0),
                      settings.wavefunction_imaginary_plot_0_name, settings)
        plot_function(system_1, 'Imaginary part of wavefunction, region 1', imag(wavefunction_1),
                      settings.wavefunction_imaginary_plot_1_name, settings)

        if settings.verbose:
            print('Wavefunction (imaginary part) plots prepared.')

            if settings.save_plots:
                print('    Wavefunction (imaginary part) plots saved.')

    if settings.show_probability_density:

        plot_function(system_0, 'Probability density, region 0', probability_density_0,
                      settings.probability_density_plot_0_name, settings)
        plot_function(system_1, 'Probability density, region 1', probability_density_1,
                      settings.probability_density_plot_1_name, settings)

        if settings.verbose:
            print('Probability density plots prepared.')

            if settings.save_plots:
                print('    Probability density plots saved.')

    if settings.record_results:
        data = [settings.gds_file, settings.distance_between_dots_nm, settings.depth_nm, settings.lattice_constant_nm,
                settings.voltage_multiplier * settings.voltages[0], settings.voltage_multiplier * settings.voltages[1],
                settings.x_side_length_nm, settings.y_side_length_nm, settings.testing_note,
                settings.convergence_factor, i, new_eigenvalue_0, new_eigenvalue_1, quadrupole_moment_xx,
                quadrupole_moment_yy, settings.external_potential, direction, settings.external_potential_component_xx,
                settings.external_potential_component_yy, quadrupole_interaction_energy, settings.total_number_of_sites,
                stopwatch(TIME_0), settings.x_over_y_ratio]

        append_to_file(data, settings.results_data_name)

        print('Results appended to file.')

    if settings.verbose:
        if system() == 'Windows':
            from winsound import Beep

            Beep(250, 250)
        elif system() == 'Linux':
            from os import system

            system('play --no-show-progress --null --channels 1 synth %s sine %f' % (0.1, 1000))

    if settings.verbose:
        print('Simulation complete after', stopwatch(TIME_0), 's.')

    if settings.show_graphs:

        if settings.verbose:
            print('Showing plots.')

        show()
