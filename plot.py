#!/usr/bin/env python
# coding=utf-8

"""
plot.py

Python code which can be run to produce plots of data stored in csv file.
"""

from csv import reader
from numpy import ceil
from matplotlib.pyplot import figure, show, savefig, title, grid, minorticks_on, xscale, gca
from matplotlib.ticker import FormatStrFormatter

from Settings import Settings

__author__ = 'Ryan Moodie'
__data__ = '14 Aug 2015'

if __name__ == '__main__':

    settings = Settings()

    with open(settings.results_data_name, 'rb') as csv_file:
        csv_data = reader(csv_file)

        headers = [header.strip(' ').replace('_', ' ') for header in csv_data.next()]

        if settings.display_available_names:
            print headers
            exit()

        rows = []

        for row in csv_data:
            rows.append(row)

    x_axis = []
    x_index = headers.index(settings.plot_x_axis)

    for row in rows:
        x_axis.append(float(row[x_index]) * settings.x_axis_factor)

    y_axis_0 = []
    y_index_0 = headers.index(settings.plot_y_axis_left)

    for row in rows:
        y_axis_0.append(float(row[y_index_0]) * settings.y_axis_factor_0)

    if settings.show_second_line:

        y_axis_right_0 = []
        y_index_right_0 = headers.index(settings.plot_y_axis_right_0)

        for row in rows:
            y_axis_right_0.append(float(row[y_index_right_0]) * settings.y_axis_factor_1)

    if settings.show_third_line:

        y_axis_right_1 = []
        y_index_right_1 = headers.index(settings.plot_y_axis_right_1)

        for row in rows:
            y_axis_right_1.append(float(row[y_index_right_1]) * settings.y_axis_factor_1)

    x_axis_numerated = []

    number_0 = 0
    for value in x_axis:
        x_axis_numerated.append([value, number_0])
        number_0 += 1

    y_axis_sorted_0 = []
    x_axis_sorted = []

    for value, number_1 in sorted(x_axis_numerated):
        y_axis_sorted_0.append(y_axis_0[number_1])
        x_axis_sorted.append(value)

    if settings.show_second_line:

        y_axis_sorted_right_0 = []

        for value, number_1 in sorted(x_axis_numerated):
            y_axis_sorted_right_0.append(y_axis_right_0[number_1])

    if settings.show_third_line:

        y_axis_sorted_right_1 = []

        for value, number_1 in sorted(x_axis_numerated):
            y_axis_sorted_right_1.append(y_axis_right_1[number_1])

    if settings.automatic_plot_title:

        plot_title = headers[y_index_0].capitalize() + ', ' + headers[y_index_right_0].capitalize() + ' and ' + headers[
            y_index_right_1] + ' against ' + headers[x_index] if settings.show_third_line else \
            headers[y_index_0].capitalize() + ' and ' + headers[y_index_right_0] + ' against ' + headers[x_index] \
                if settings.show_second_line else headers[y_index_0].capitalize() + ' against ' + headers[x_index]

    else:

        plot_title = settings.manual_plot_title

    file_name = 'png/' + plot_title + settings.png_suffix + '.pdf'

    figure(1)
    axis_0 = gca()

    if settings.logarithmic:
        xscale('log')
        axis_0.xaxis.set_minor_formatter(FormatStrFormatter("%.1f"))
        axis_0.xaxis.set_major_formatter(FormatStrFormatter("%.0f"))
        axis_0.tick_params(axis='both', which='major', labelsize=14)
        axis_0.tick_params(axis='both', which='minor', labelsize=11)

    if settings.set_origin_x:
        x_max = max(x_axis_sorted)
        axis_0.set_xlim(0, x_max)

    elif settings.force_tight_x:
        x_min = (min(x_axis_sorted))
        x_max = ceil(max(x_axis_sorted))
        axis_0.set_xlim(x_min, x_max)

    if settings.set_origin_y_0:
        y_max = ceil(
            max(y_axis_sorted_0 + y_axis_sorted_right_0 + y_axis_sorted_right_1) if settings.show_third_line and not
            settings.show_second_y_axis else
            max(
                y_axis_sorted_0 + y_axis_sorted_right_0) if settings.show_second_line and not settings.show_second_y_axis else
            max(y_axis_sorted_0))
        axis_0.set_ylim(0, y_max)

    minorticks_on()

    if settings.show_grid:
        grid(b=True, which='minor', axis='both', linestyle='-', color='0.8')
        grid(b=True, which='major', axis='both', linestyle='-', color='0.4')

    if settings.show_title:
        title(plot_title)

    x_axis_label = settings.plot_x_axis if settings.automatic_x_axis_label else settings.x_axis_label

    if settings.automatic_y_axis_label_0:

        y_axis_label_0 = (
            headers[y_index_0].capitalize() + ' / ' + headers[y_index_right_0] + ' / ' + headers[y_index_right_1] if
            settings.show_third_line and not settings.show_second_y_axis else
            headers[y_index_0].capitalize() + ' / ' + headers[y_index_right_0] if
            settings.show_second_line and not settings.show_second_y_axis else
            headers[y_index_0].capitalize()
        )

    else:
        y_axis_label_0 = settings.y_axis_label_0

    axis_0.set_xlabel(x_axis_label)

    y_label_left_color = 'red' if settings.show_second_y_axis and not settings.show_third_line else 'black'

    axis_0.set_ylabel(y_axis_label_0, color=y_label_left_color)

    line_0 = axis_0.plot(x_axis_sorted, y_axis_sorted_0, markerfacecolor='blue', markeredgecolor='blue', marker='o',
                         linestyle='none', markersize=4)

    if settings.show_second_line:

        if settings.show_second_y_axis:

            axis_1 = axis_0.twinx()

            if settings.automatic_y_axis_label_right:

                y_axis_label_right = headers[y_index_right_0].capitalize()

            else:

                y_axis_label_right = settings.y_axis_label_right

            line_1 = axis_1.plot(x_axis_sorted, y_axis_sorted_right_0, markerfacecolor='red', markeredgecolor='red',
                                 marker='o', linestyle='none', markersize=4)

            y_label_right_color = 'black' if settings.show_third_line else 'blue'

            axis_1.set_ylabel(y_axis_label_right, color=y_label_right_color)

            if settings.show_third_line:
                line_2 = axis_1.plot(x_axis_sorted, y_axis_sorted_right_1, markerfacecolor='green',
                                     markeredgecolor='green', marker='o', linestyle='none', markersize=4)

            if settings.set_origin_y_right:
                y_max = max(y_axis_sorted_right_0 + y_axis_sorted_right_1) if settings.show_third_line else max(
                    y_axis_sorted_right_0)

                axis_1.set_ylim(0, y_max)

        else:

            line_1 = axis_0.plot(x_axis_sorted, y_axis_sorted_right_0, markerfacecolor='red', markeredgecolor='red',
                                 marker='o', linestyle='none', markersize=4)

            if settings.show_third_line:
                line_2 = axis_0.plot(x_axis_sorted, y_axis_sorted_right_1, markerfacecolor='green',
                                     markeredgecolor='green', marker='o', linestyle='none', markersize=4)

    if settings.show_legend:
        axis_0.legend(
            line_0 + line_1 + line_2 if settings.show_third_line else line_0 + line_1 if settings.show_second_line else
            line_0, [headers[y_index_0].capitalize() if settings.automatic_legend_label_0 else
                     settings.legend_label_0,
                     headers[y_index_right_0].capitalize() if settings.automatic_legend_label_right_0 else
                     settings.legend_label_right_0,
                     headers[y_index_right_1].capitalize() if settings.automatic_legend_label_right_1 else
                     settings.legend_label_right_1], loc='best', numpoints=1)

    if settings.save_plots:
        savefig(file_name, bbox_inches='tight', format='pdf', transparent=True)

    if settings.show_plots:
        show()
