# coding=utf-8

"""
Simulation.py
Solves for initial potential.
"""

from __future__ import division

from numpy import ogrid, array, linspace
from matplotlib.pyplot import title, xlabel, ylabel, axis, xticks, yticks, fill, text, figure, savefig

from PointlistFromGds import PointlistFromGds
from PolygonPotentials import Polygon

__author__ = "Hanno Flentje and Ryan Moodie"
__date__ = "11 Aug 2015"


class Simulation(object):
    def __init__(self):
        self.potential = array
        self.gate_dictionary = {}
        self.gds_file = str
        self.gate_array = list
        self.gates = list
        self.potential_F = array

    def call_gates(self, settings):
        gates = []
        for i, a in enumerate(self.gate_array):
            gates.append(Polygon(self.gate_dictionary[str(i)], settings.depth_m, a))
        return gates

    def evaluate_potential(self, settings):
        self.gds_file = settings.gds_file
        self.gate_array = PointlistFromGds(self.gds_file).run()
        for i, val in enumerate(settings.voltages):
            self.gate_dictionary[str(i)] = settings.voltage_multiplier * val
        self.gates = self.call_gates(settings)
        # p is an 2D array which contains all sites. Here we use broadcasting.
        p = array(ogrid[settings.left_border_nm:settings.right_border_nm:settings.lattice_constant_nm,
                  settings.lower_border_nm:settings.upper_border_nm:settings.lattice_constant_nm]) * 1e-9

        # Calculating the potential for each site. In FORTRAN order initially (rows, columns):
        self.potential_F = sum(gate(p) for gate in self.gates)
        # Transposed to C order (columns, rows):
        self.potential = self.potential_F.T

    def plot_gates(self, settings):
        figure(settings.n)
        title('Gate Structure')
        xlabel('x [nm]')
        ylabel('y [nm]')
        axis([settings.left_border_m, settings.right_border_m, settings.lower_border_m, settings.upper_border_m])
        xticks(linspace(settings.left_border_m, settings.right_border_m, 9),
               linspace(settings.left_border_nm, settings.right_border_nm, 9))
        yticks(linspace(settings.lower_border_m, settings.upper_border_m, 9),
               linspace(settings.lower_border_nm, settings.upper_border_nm, 9))

        gates = self.call_gates(settings)
        for i, gate_array in enumerate(self.gate_array):
            x_position = gate_array[:, 0]
            y_position = gate_array[:, 1]
            fill(x_position, y_position, facecolor='b', alpha=0.5, edgecolor='black')
            text(gates[i].points_x[0], gates[i].points_y[0], str(i))

        if settings.save_plots:
            savefig(settings.gates_plot_name, bbox_inches='tight')

        settings.end_figure()

    def read_potential(self, data):
        self.potential = data
