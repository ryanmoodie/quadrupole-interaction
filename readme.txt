Solves for a 2DEG parallel to a two dimensional gate structure at a defined depth from the gate structure in vacuum.
Solves potential of 2DEG, then solves for wavefunctions in two regions in the 2DEG by a self-consistent method.
Calculates the quadrupole interaction between these two wavefunctions, eg. electrons.

Configure settings of simulation in settings.cfg with a text editor.

File to be executed to run simulation is main.py. Run from terminal by setting the simulation folder as your current
directory then entering 'python2 main.py' or 'python main.py' (depending on your setup).

Notes:
Positive test charges are used in the simulation, although quantities are defined for electrons. Simply treat the
output potential with a minus sign.
When defining state, 0 is the ground state, 1 is the first excited state, etc.

Uses Python 2.7.10

Package dependancies (pip names):
kwant
matplotlib
numpy
scipy
configparser
python-gdsii
pickle
scikit-umfpack (optional)
