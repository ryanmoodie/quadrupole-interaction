# coding=utf-8

"""
QuadrupoleInteraction.py
Calculates the quadrupole interaction between the two electrons in the simulation.
"""

from __future__ import division

from numpy import sqrt

__author__ = 'Ryan Moodie'
__credits__ = ["Giuseppe Pica", "Dr Brendon Lovett"]
__date__ = '10 Aug 2015'


def find_quadrupole_moment_component(axis, probability_density_matrix_0, probability_density_matrix_1, settings):
    def add_region_contribution(_quadrupole_moment_component, probability_density_matrix, number):

        def spatial_function(_x, _y):
            if axis == 'xx':
                return 2 * _x ** 2 - _y ** 2
            elif axis == 'yy':
                return 2 * _y ** 2 - _x ** 2

        if number:
            x_axis = settings.x_axis_quantum_nm_1
            y_axis = settings.y_axis_quantum_nm_1
        else:
            x_axis = settings.x_axis_quantum_nm_0
            y_axis = settings.y_axis_quantum_nm_0

        for y in y_axis:
            for x in x_axis:
                a = settings.convert_x_position_to_grid(x, number)
                b = settings.convert_y_position_to_grid(y, number)
                x *= 1e-9
                y *= 1e-9
                _quadrupole_moment_component += spatial_function(x, y) * probability_density_matrix[b, a]

        return _quadrupole_moment_component

    quadrupole_moment_component = float(0)

    quadrupole_moment_component = add_region_contribution(quadrupole_moment_component, probability_density_matrix_0, 0)
    quadrupole_moment_component = add_region_contribution(quadrupole_moment_component, probability_density_matrix_1, 1)

    return quadrupole_moment_component


def find_quadrupole_interaction_energy_automatic(quadrupole_moment_xx, quadrupole_moment_yy, settings):
    if abs(quadrupole_moment_xx) > abs(quadrupole_moment_yy):
        moment_function = 2 * quadrupole_moment_xx + quadrupole_moment_yy
        direction = 'Vxx'
    else:
        moment_function = 2 * quadrupole_moment_yy + quadrupole_moment_xx
        direction = 'Vyy'

    return direction, settings.external_potential * moment_function / 6


def find_quadrupole_interaction_energy_manual(quadrupole_moment_xx, quadrupole_moment_yy, settings):
    return (settings.external_potential_component_xx * (
        2 * quadrupole_moment_xx + quadrupole_moment_yy) + settings.external_potential_component_yy * (
                2 * quadrupole_moment_yy + quadrupole_moment_xx)) / 6
