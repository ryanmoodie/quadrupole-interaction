# coding=utf-8

"""
Settings.py
Reads settings from settings.cfg file input by user and calculates some parameters.
"""

from __future__ import division

from numpy import arange, round, sqrt
from scipy.constants import m_e, hbar, e, pi, epsilon_0
import ConfigParser

__author__ = "Ryan Moodie"
__date__ = "14 Aug 2015"


class Settings(object):
    def convert_x_position_to_grid(self, position_nm, number):
        if number == 1:
            left_border_nm = self.start_x_nm_1
        elif number == 0:
            left_border_nm = self.start_x_nm_0
        elif number == 'potential':
            left_border_nm = self.left_border_nm

        return int(round(abs(position_nm - left_border_nm) / self.lattice_constant_nm, 0))

    def convert_y_position_to_grid(self, position_nm, number):
        if number == 1:
            lower_border_nm = self.start_y_nm_1
        elif number == 0:
            lower_border_nm = self.start_y_nm_0
        elif number == 'potential':
            lower_border_nm = self.lower_border_nm

        return int(round(abs(position_nm - lower_border_nm) / self.lattice_constant_nm, 0))

    def __init__(self):
        config = ConfigParser.RawConfigParser()
        config.read('settings.cfg')

        self.verbose = config.getboolean('General', 'verbose')
        self.record_results = config.getboolean('General', 'record_results')
        self.testing_note = config.get('General', 'testing_note')

        self.depth_nm = config.getfloat('General', 'depth_nm')
        self.depth_m = self.depth_nm * 1e-9

        lattice_constant_nm_input = config.get('General', 'lattice_constant_nm')
        self.lattice_constant_nm = float(eval(lattice_constant_nm_input))
        self.lattice_constant_m = self.lattice_constant_nm * 1e-9

        if config.getboolean('AutomaticSchrodingerSolvingRegions', 'automatically_define_schrodinger_solving_regions'):

            x_over_y_ratio_input = config.get('AutomaticSchrodingerSolvingRegions', 'x_over_y_ratio')
            self.x_over_y_ratio = float(eval(x_over_y_ratio_input))

            self.distance_between_dots_nm = config.getfloat('AutomaticSchrodingerSolvingRegions',
                                                            'distance_between_dots_nm')

            if config.getboolean('AutomaticSchrodingerSolvingRegions', 'choose_solving_region_by_number_of_sites'):

                number_of_sites_in_one_region = config.getint('AutomaticSchrodingerSolvingRegions',
                                                              'number_of_sites_in_one_region')

                self.x_side_length_nm = sqrt(
                    number_of_sites_in_one_region * self.x_over_y_ratio) * self.lattice_constant_nm
                self.y_side_length_nm = sqrt(
                    number_of_sites_in_one_region / self.x_over_y_ratio) * self.lattice_constant_nm

            else:
                self.x_side_length_nm = config.getfloat('AutomaticSchrodingerSolvingRegions', 'x_side_length_nm')
                self.y_side_length_nm = self.x_side_length_nm / self.x_over_y_ratio

            half_x_side_length_nm = self.x_side_length_nm / 2
            half_y_side_length_nm = self.y_side_length_nm / 2
            half_distance_between_dots_nm = self.distance_between_dots_nm / 2

            self.start_x_nm_0 = - half_distance_between_dots_nm - half_x_side_length_nm
            self.end_x_nm_0 = - half_distance_between_dots_nm + half_x_side_length_nm
            self.start_y_nm_0 = - half_y_side_length_nm
            self.end_y_nm_0 = half_y_side_length_nm

            self.start_x_nm_1 = half_distance_between_dots_nm - half_x_side_length_nm
            self.end_x_nm_1 = half_distance_between_dots_nm + half_x_side_length_nm
            self.start_y_nm_1 = - half_y_side_length_nm
            self.end_y_nm_1 = half_y_side_length_nm

        else:

            self.start_x_nm_0 = config.getfloat('SchrodingerSolvingRegion0', 'start_x_nm')
            self.end_x_nm_0 = config.getfloat('SchrodingerSolvingRegion0', 'end_x_nm')
            self.start_y_nm_0 = config.getfloat('SchrodingerSolvingRegion0', 'start_y_nm')
            self.end_y_nm_0 = config.getfloat('SchrodingerSolvingRegion0', 'end_y_nm')

            self.start_x_nm_1 = config.getfloat('SchrodingerSolvingRegion1', 'start_x_nm')
            self.end_x_nm_1 = config.getfloat('SchrodingerSolvingRegion1', 'end_x_nm')
            self.start_y_nm_1 = config.getfloat('SchrodingerSolvingRegion1', 'start_y_nm')
            self.end_y_nm_1 = config.getfloat('SchrodingerSolvingRegion1', 'end_y_nm')

        self.n = 1

        # hbar^2 / (2 x m* x a^2) x 1000/e ; m* = transverse effective mass = 0.191 x mass_electron
        self.hopping_constant_meV = 1e3 * hbar ** 2 / (e * 0.382 * m_e * self.lattice_constant_m ** 2)

        # 1 / (4 x pi x epsilon_0 x epsilon_silicon)
        self.coulomb_factor = 1 / (pi * epsilon_0 * 46.72)

        # e^2 / (4 x pi x epsilon_0 x epsilon_silicon) x 1000/e x 1e9 (since r in nm)
        self.new_potential_factor_meV = e * 1e12 * self.coulomb_factor

        calculate_and_save = config.getboolean('QuickOptions', 'calculate_all_and_save_data')
        calculate_only = config.getboolean('QuickOptions', 'calculate_all_without_saving')
        load_only = config.getboolean('QuickOptions', 'load_all')

        if calculate_and_save or calculate_only or load_only:

            if calculate_and_save:
                load = False
                save = True

            elif calculate_only:
                load = False
                save = False

            elif load_only:
                load = True
                save = False

            self.load_potential = load
            self.save_potential = save
            self.load_distances_array = load
            self.save_distances_array = save
            self.load_self_consistent_results = load
            self.save_wavefunctions = save
            self.load_quadrupole_interaction = load

        else:

            self.load_potential = config.getboolean('InitialPoissonSolver', 'load_potential')
            self.save_potential = config.getfloat('InitialPoissonSolver', 'save_potential')
            self.load_distances_array = config.getboolean('SelfConsistentSolver', 'load_distances_array')
            self.save_distances_array = config.getboolean('SelfConsistentSolver', 'save_distances_array')
            self.load_self_consistent_results = config.getboolean('SelfConsistentSolutions', 'load_results')
            self.save_wavefunctions = config.getboolean('SelfConsistentSolutions', 'save_results')

        self.show_potential_only = config.getboolean('QuickOptions', 'check_initial_potential_and_solving_boundaries')

        self.gds_file = config.get('InitialPoissonSolver', 'gds_file')

        self.show_gates = config.getboolean('InitialPoissonSolver', 'show_gates')

        self.voltage_multiplier = config.getfloat('Voltages', 'voltage_multiplier')
        self.voltages = [0] * 10
        self.voltages[0] = config.getfloat('Voltages', 'gate0_V')
        self.voltages[1] = config.getfloat('Voltages', 'gate1_V')
        self.voltages[2] = config.getfloat('Voltages', 'gate2_V')
        self.voltages[3] = config.getfloat('Voltages', 'gate3_V')
        self.voltages[4] = config.getfloat('Voltages', 'gate4_V')
        self.voltages[5] = config.getfloat('Voltages', 'gate5_V')
        self.voltages[6] = config.getfloat('Voltages', 'gate6_V')
        self.voltages[7] = config.getfloat('Voltages', 'gate7_V')
        self.voltages[8] = config.getfloat('Voltages', 'gate8_V')
        self.voltages[9] = config.getfloat('Voltages', 'gate9_V')

        if config.getboolean('InitialPoissonSolvingRegion', 'automatic'):

            self.left_border_nm = min(self.start_x_nm_0, self.start_x_nm_1)
            self.right_border_nm = max(self.end_x_nm_0, self.end_x_nm_1) + self.lattice_constant_nm
            self.lower_border_nm = min(self.start_y_nm_0, self.start_y_nm_1)
            self.upper_border_nm = max(self.end_y_nm_0, self.end_y_nm_1) + self.lattice_constant_nm

        else:

            self.left_border_nm = config.getfloat('InitialPoissonSolvingRegion', 'left_x_border_nm')
            self.right_border_nm = config.getfloat('InitialPoissonSolvingRegion',
                                                   'right_x_border_nm') + self.lattice_constant_nm
            self.lower_border_nm = config.getfloat('InitialPoissonSolvingRegion', 'lower_y_border_nm')
            self.upper_border_nm = config.getfloat('InitialPoissonSolvingRegion',
                                                   'upper_y_border_nm') + self.lattice_constant_nm

        self.left_border_m = self.left_border_nm * 1e-9
        self.right_border_m = self.right_border_nm * 1e-9
        self.lower_border_m = self.lower_border_nm * 1e-9
        self.upper_border_m = self.upper_border_nm * 1e-9

        self.start_x_grid_0 = 0
        self.end_x_grid_0 = self.convert_x_position_to_grid(self.end_x_nm_0, 0)
        self.start_y_grid_0 = 0
        self.end_y_grid_0 = self.convert_y_position_to_grid(self.end_y_nm_0, 0)

        self.start_x_grid_1 = 0
        self.end_x_grid_1 = self.convert_x_position_to_grid(self.end_x_nm_1, 1)
        self.start_y_grid_1 = 0
        self.end_y_grid_1 = self.convert_y_position_to_grid(self.end_y_nm_1, 1)

        self.start_x_potential_grid_0 = self.convert_x_position_to_grid(self.start_x_nm_0, 'potential')
        self.end_x_potential_grid_0 = self.convert_x_position_to_grid(self.end_x_nm_0, 'potential')
        self.start_y_potential_grid_0 = self.convert_y_position_to_grid(self.start_y_nm_0, 'potential')
        self.end_y_potential_grid_0 = self.convert_y_position_to_grid(self.end_y_nm_0, 'potential')

        self.start_x_potential_grid_1 = self.convert_x_position_to_grid(self.start_x_nm_1, 'potential')
        self.end_x_potential_grid_1 = self.convert_x_position_to_grid(self.end_x_nm_1, 'potential')
        self.start_y_potential_grid_1 = self.convert_y_position_to_grid(self.start_y_nm_1, 'potential')
        self.end_y_potential_grid_1 = self.convert_y_position_to_grid(self.end_y_nm_1, 'potential')

        self.row_size_nm_0 = self.end_x_nm_0 - self.start_x_nm_0
        self.column_size_nm_0 = self.end_y_nm_0 - self.start_y_nm_0
        self.row_size_grid_0 = int(self.end_x_grid_0 + 1)
        self.column_size_grid_0 = int(self.end_y_grid_0 + 1)

        self.row_size_nm_1 = self.end_x_nm_1 - self.start_x_nm_1
        self.column_size_nm_1 = self.end_y_nm_1 - self.start_y_nm_1
        self.row_size_grid_1 = int(self.end_x_grid_1 + 1)
        self.column_size_grid_1 = int(self.end_y_grid_1 + 1)

        self.number_of_sites_0 = self.row_size_grid_0 * self.column_size_grid_0
        self.number_of_sites_1 = self.row_size_grid_1 * self.column_size_grid_1
        self.total_number_of_sites = self.number_of_sites_0 + self.number_of_sites_1

        self.x_axis_nm = arange(self.left_border_nm, self.right_border_nm, self.lattice_constant_nm)
        self.y_axis_nm = arange(self.lower_border_nm, self.upper_border_nm, self.lattice_constant_nm)

        self.x_axis_quantum_nm_0 = arange(self.start_x_nm_0, self.end_x_nm_0, self.lattice_constant_nm)
        self.y_axis_quantum_nm_0 = arange(self.start_y_nm_0, self.end_y_nm_0, self.lattice_constant_nm)

        self.x_axis_quantum_nm_1 = arange(self.start_x_nm_1, self.end_x_nm_1, self.lattice_constant_nm)
        self.y_axis_quantum_nm_1 = arange(self.start_y_nm_1, self.end_y_nm_1, self.lattice_constant_nm)

        self.convergence_factor = config.getfloat('SelfConsistentSolver', 'convergence_factor')
        self.state_0 = config.getint('SelfConsistentSolver', 'state_0')
        self.state_1 = config.getint('SelfConsistentSolver', 'state_1')

        self.show_probability_density_matrix = config.getboolean('SelfConsistentSolutions',
                                                                 'show_probability_densities')

        self.show_eigenvalues = config.getboolean('SelfConsistentSolutions', 'show_eigenvalues')

        self.save_plots = config.getboolean('Plots', 'save_plots')
        self.show_solving_region_plot = config.getboolean('Plots', 'show_potential_plot_with_solving_regions')
        self.show_probability_density = config.getboolean('Plots', 'show_probability_densities')
        self.show_wavefunction_real = config.getboolean('Plots', 'show_wavefunctions_real')
        self.show_wavefunction_imaginary = config.getboolean('Plots', 'show_wavefunctions_imaginary')
        self.show_probability_densities_for_iterations = config.getboolean('Plots',
                                                                           'show_probability_densities_for_iterations')

        self.show_plots = config.getboolean('Plots', 'show_plots')

        self.show_graphs = self.show_plots and self.show_gates or self.show_solving_region_plot or \
                           self.show_wavefunction_real or self.show_wavefunction_imaginary or \
                           self.show_probability_density

        self.calculate_quadrupole_interaction = config.getboolean('QuadrupoleInteraction',
                                                                  'calculate_quadrupole_interaction')
        self.show_quadrupole_interaction_results = config.getboolean('QuadrupoleInteraction',
                                                                     'show_quadrupole_interaction_results')
        self.align_external_potential_with_strongest_moment = config.getboolean('QuadrupoleInteraction',
                                                                                'align_external_potential_'
                                                                                'with_strongest_moment')
        self.external_potential = config.getfloat('QuadrupoleInteraction', 'external_potential')
        self.external_potential_component_xx = config.getfloat('QuadrupoleInteraction',
                                                               'external_potential_component_xx')
        self.external_potential_component_yy = config.getfloat('QuadrupoleInteraction',
                                                               'external_potential_component_yy')

        self.csv_suffix = config.get('FileNames', 'csv_suffix')
        self.pkl_suffix = config.get('FileNames', 'pkl_suffix')
        self.png_suffix = config.get('FileNames', 'png_suffix')

        self.results_data_name = 'csv/results_data' + self.csv_suffix + '.csv'

        self.potential_name = 'pkl/initial_potential' + self.pkl_suffix + '.pkl'
        self.distances_array_name = 'pkl/distances_arrays' + self.pkl_suffix + '.pkl'
        self.eigenvalues_name = 'pkl/eigenvalues' + self.pkl_suffix + '.pkl'
        self.wavefunctions_name = 'pkl/wavefunctions' + self.pkl_suffix + '.pkl'
        self.probability_densities_name = 'pkl/probability_densities' + self.pkl_suffix + '.pkl'
        self.probability_density_matrices_name = 'pkl/probability_density_matrices' + self.pkl_suffix + '.pkl'

        self.gates_plot_name = 'png/gates' + self.png_suffix + '.png'
        self.potential_plot_name = 'png/potential' + self.png_suffix + '.png'
        self.probability_density_plot_0_name = 'png/probability_density_0' + self.png_suffix + '.png'
        self.probability_density_plot_1_name = 'png/probability_density_1' + self.png_suffix + '.png'
        self.wavefunction_real_plot_0_name = 'png/wavefunction_real_0' + self.png_suffix + '.png'
        self.wavefunction_real_plot_1_name = 'png/wavefunction_real_1' + self.png_suffix + '.png'
        self.wavefunction_imaginary_plot_0_name = 'png/wavefunction_imaginary_0' + self.png_suffix + '.png'
        self.wavefunction_imaginary_plot_1_name = 'png/wavefunction_imaginary_1' + self.png_suffix + '.png'

        self.display_available_names = config.getboolean('ResultsPlot', 'display_available_names')

        self.plot_x_axis = config.get('ResultsPlot', 'x_axis')
        self.plot_y_axis_left = config.get('ResultsPlot', 'y_axis_left')
        self.plot_y_axis_right_0 = config.get('ResultsPlot', 'y_axis_right_0')
        self.show_second_line = False if self.plot_y_axis_right_0 == '' else True

        self.plot_y_axis_right_1 = config.get('ResultsPlot', 'y_axis_right_1')
        self.show_third_line = False if self.plot_y_axis_right_1 == '' else True

        self.show_title = config.getboolean('ResultsPlot', 'show_title')

        x_axis_factor_str = config.get('ResultsPlot', 'x_axis_factor')
        self.x_axis_factor = 1 if x_axis_factor_str == '' else float(x_axis_factor_str)

        y_axis_factor_0_str = config.get('ResultsPlot', 'y_axis_factor_left')
        self.y_axis_factor_0 = 1 if y_axis_factor_0_str == '' else float(y_axis_factor_0_str)

        if self.show_second_line:

            y_axis_factor_1_str = config.get('ResultsPlot', 'y_axis_factor_right')
            self.y_axis_factor_1 = 1 if y_axis_factor_1_str == '' else float(y_axis_factor_1_str)

            self.legend_label_right_0 = config.get('ResultsPlot', 'legend_label_right_0')
            self.automatic_legend_label_right_0 = True if self.legend_label_right_0 == '' else False

        if self.show_third_line:

            self.legend_label_right_1 = config.get('ResultsPlot', 'legend_label_right_1')
            self.automatic_legend_label_right_1 = True if self.legend_label_right_1 == '' else False

        self.legend_label_0 = config.get('ResultsPlot', 'legend_label_left')
        self.automatic_legend_label_0 = True if self.legend_label_0 == '' else False

        self.manual_plot_title = config.get('ResultsPlot', 'plot_title')
        self.automatic_plot_title = True if self.manual_plot_title == '' else False

        self.x_axis_label = config.get('ResultsPlot', 'x_axis_label')
        self.automatic_x_axis_label = True if self.x_axis_label == '' else False
        self.y_axis_label_0 = config.get('ResultsPlot', 'y_axis_label_left')
        self.automatic_y_axis_label_0 = True if self.y_axis_label_0 == '' else False
        self.y_axis_label_right = config.get('ResultsPlot', 'y_axis_label_right')
        self.automatic_y_axis_label_right = True if self.y_axis_label_right == '' else False

        self.show_grid = config.getboolean('ResultsPlot', 'show_grid')
        self.set_origin_x = config.getboolean('ResultsPlot', 'zero_origin_x')
        self.set_origin_y_0 = config.getboolean('ResultsPlot', 'zero_origin_y_left')
        set_origin_y_right_str = config.get('ResultsPlot', 'zero_origin_y_right')
        self.set_origin_y_right = False if set_origin_y_right_str == '' else bool(set_origin_y_right_str)
        self.logarithmic = config.getboolean('ResultsPlot', 'logarithmic')
        self.force_tight_x = config.getboolean('ResultsPlot', 'force_tight_x')
        self.show_legend = config.getboolean('ResultsPlot', 'show_legend')
        self.show_second_y_axis = config.getboolean('ResultsPlot', 'show_second_y_axis')

    def iteration_probability_densities_names(self, i, number):
        return 'png/iteration_' + str(i) + '_probability_density_' + str(number) + self.png_suffix + '.png'

    def end_figure(self):
        self.n += 1
