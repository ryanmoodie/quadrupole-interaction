# coding=utf-8

"""
ReadWriteToDisk.py
Defines functions to write out data to disk and read data from disk.
"""

from csv import writer
from cPickle import dump, load

__author__ = "Ryan Moodie"
__date__ = "14 Aug 2015"


def write_out(data, pkl_file):
    with open(pkl_file, 'wb') as output_file:
        dump(data, output_file)


def read_file(pkl_file):
    with open(pkl_file, 'rb') as input_file:
        return load(input_file)


def append_to_file(data, csv_file):
    with open(csv_file, 'a') as data_file:
        data_write = writer(data_file)
        data_write.writerow(data)
